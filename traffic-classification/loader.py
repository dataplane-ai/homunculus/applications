# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import sys
import numpy as np
import csv

# Loader for (modified) TMC IoT Dataset - https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/tmc-iot-traces

train_pct = 0.8

# 0-Static Smart Home Devices
# 1-Sensors
# 2-Audio
# 3-Video
# 4-Other
# 5-Remove
classes = {"d0:52:a8:00:67:5e": 0,  # Smart Things
           "44:65:0d:56:cc:d3": 2,  # Amazon Echo
           "70:ee:50:18:34:43": 3,  # Netatmo Welcome
           "f4:f2:6d:93:51:f1": 3,  # TP-Link Day Night Cloud Camera
           "00:16:6c:ab:6b:88": 3,  # Samsung SmartCam
           "30:8c:fb:2f:e4:b2": 3,  # Dropcam
           "00:62:6e:51:27:2e": 3,  # Insteon Camera
           "00:24:e4:11:18:a8": 1,  # Withings Smart Baby Monitor
           "ec:1a:59:79:f4:89": 0,  # Belkin Wemo Switch
           "50:c7:bf:00:56:39": 0,  # TP-Link Smart Plug
           "74:c6:3b:29:d7:1d": 2,  # iHome
           "ec:1a:59:83:28:11": 1,  # Belkin Wemo Motion Sensor
           "18:b4:30:25:be:e4": 1,  # NEST Protect Smoke Alarm
           "70:ee:50:03:b8:ac": 1,  # Netatmo Weather Station
           "00:24:e4:1b:6f:96": 1,  # Withings Smart Scale
           "74:6a:89:00:2e:25": 1,  # Blipcare Blood Pressure Meter
           "00:24:e4:20:28:c6": 1,  # Withings Aura Smart Sleep Sensor
           "d0:73:d5:01:83:08": 0,  # Light Bulbs LiFX Smart Bulb
           "18:b7:9e:02:20:44": 2,  # Triby Speaker
           "e0:76:d0:33:bb:85": 3,  # PIX-STAR Photo-Frame
           "70:5a:0f:e4:9b:c0": 4,  # HP Printer
           "08:21:ef:3b:fc:e3": 4,  # Samsung Galaxy Tab
           "30:8c:fb:b6:ea:45": 3,  # Nest Dropcam
           "40:f3:08:ff:1e:da": 4,  # Android Phone
           "74:2f:68:81:69:42": 4,  # Laptop
           "ac:bc:32:d4:6f:2f": 4,  # MacBook
           "b4:ce:f6:a7:a3:c2": 4,  # Android Phone
           "d0:a6:37:df:a1:e1": 4,  # Iphone
           "f4:5c:89:93:cc:85": 4,  # Macbook/Iphone
           "14:cc:20:51:33:ea": 4}  # TPLink Router Bridge LAN (Gateway)

EthTypes = ["", "802.1X Authentication", "ARP", "IEEE 802.11 data encapsulation", "IPv4", "IPv6"]
EthType_Dict = dict(zip(EthTypes, range(len(EthTypes))))

IPv4Protos = ["", "ICMP,ICMP", "ICMP,TCP", "ICMP,UDP", "ICMP", "IGMP", "TCP", "UDP"]
IPv4Protos_Dict = dict(zip(IPv4Protos, range(len(IPv4Protos))))

IPv4Flags = ["", "0x0000", "0x00b9", "0x2000", "0x4000", "0x0000,0x0000", "0x0000,0x2000", "0x0000,0x4000"]
IPv4Flags_Dict = dict(zip(IPv4Flags, range(len(IPv4Flags))))

IPv6Next = ["", "Fragment Header for IPv6", "ICMPv6", "IPv6 Hop-by-Hop Option", "TCP", "UDP"]
IPv6Next_Dict = dict(zip(IPv6Next, range(len(IPv6Next))))

IPv6Opt = ["", "1"]
IPv6Opt_Dict = dict(zip(IPv6Opt, range(len(IPv6Opt))))

TCPFlags = ["", "0x002", "0x004", "0x010", "0x011", "0x012", "0x014", "0x018", "0x019", "0x0c2", "0x052", "0x001", "0x090", "0x098"]
TCPFlags_Dict = dict(zip(TCPFlags, range(len(TCPFlags))))


def load_data(dataset):
    full_data = []
    full_labels = []
    limit = 1000000
    bins = [0, 0, 0, 0, 0]
    with open(dataset, newline='\n') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        print("Reading in data")
        i = 0

        for row in spamreader:
            label = row.pop(1)
            dst, src = label.split(",")

            if (bins[classes[src]] == limit):
                continue

            bins[classes[src]] += 1

            full_labels.append(label)
            full_data.append(row)
            i = i + 1

    def label_multiclass(label):
        dst, src = label.split(",")

        new_label = [0, 0, 0, 0, 0]
        if (classes[src] == 0):
            new_label = [1, 0, 0, 0, 0]
        elif (classes[src] == 1):
            new_label = [0, 1, 0, 0, 0]
        elif (classes[src] == 2):
            new_label = [0, 0, 1, 0, 0]
        elif (classes[src] == 3):
            new_label = [0, 0, 0, 1, 0]
        elif (classes[src] == 4):
            new_label = [0, 0, 0, 0, 1]

        return new_label

    processed_labels = list(map(lambda label: label_multiclass(label), full_labels))

    def categorize_full(data_pt):

        new_pt = [0] * 7
        new_pt[0] = int(data_pt[1])  # Pkt Size
        new_pt[1] = EthType_Dict[data_pt[2]]
        new_pt[2] = IPv4Protos_Dict[data_pt[3]]
        new_pt[3] = IPv4Flags_Dict[data_pt[4]]
        new_pt[4] = IPv6Next_Dict[data_pt[5]]
        new_pt[5] = IPv6Opt_Dict[data_pt[6]]

        if (data_pt[7] == ""):
            new_pt[6] = 0
        else:
            new_pt[6] = int(data_pt[7])

        return new_pt

    cleaned_data = list(map(lambda data_pt: categorize_full(data_pt), full_data))
    temp_data = list(zip(cleaned_data, processed_labels))

    data, labels = list(zip(*temp_data))
    data = list(data)
    labels = list(labels)

    train_start = 0
    train_stop = int(train_pct * len(data))
    test_start = train_stop
    test_stop = len(data)

    trainX = data[train_start:train_stop]
    trainY = labels[train_start:train_stop]

    testX = data[test_start:test_stop]
    testY = labels[test_start:test_stop]

    return trainX, trainY, testX, testY, len(trainX[0])
