# Traffic Classification

To classify packets based on the TMC IoT Dataset

## Usage

- From within the Python3 virtual environment, run the following commands:
```
cd applications/traffic-classification/
python main.py \
    --dataset ~/datasets/tmc-iot-traces/tmc-med.csv \
    --backend ~/backends/taurus-asic/ \
    --backend-type TaurusASIC
```

- On successful completion, it will print following output:
```
[info] All tests passed.
[success] Total time: 127 s, completed Jan 31, 2023 10:08:06 AM
Tst traffic_classification_dnn_1_0 succeeded:False PCU:50 PMU:84 genpir gentst [86s] make p2p proute hybrid
PCUs: 50
PMUs: 84
Model score: 60.234177635371935
Current optimum: 71.9309872282738
HyperMapper optimization finished
Current optimum: 71.9309872282738
```


