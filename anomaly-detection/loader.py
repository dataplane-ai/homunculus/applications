
# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************


from __future__ import division, print_function, absolute_import
from itertools import accumulate
import numpy as np
import csv

# Loader for (modified) NSL KDD Dataset - https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/kdd-synthetic-traces

def load_from_file(dataset):
    full_data = []
    full_labels = []
    with open(dataset, newline='\n') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            label = []
            label.append(row.pop(-2))
            label.append(row.pop(-1))
            full_labels.append(label)
            full_data.append(row)
            
    return full_data, full_labels
