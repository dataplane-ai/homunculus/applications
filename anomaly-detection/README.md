# Anomaly Detection

To identify anomalous packets using the NSL KDD Dataset.

## Usage

- From within the Python3 virtual environment, run the following commands:
```
cd applications/anomaly-detection/
python main.py \
    --train-dataset ~/datasets/kdd-synthetic-traces/train_ad.csv \
    --test-dataset ~/datasets/kdd-synthetic-traces/test_ad.csv \
    --backend ~/backends/taurus-asic/ \
    --backend-type TaurusASIC
```

- On successful completion, it will print following output:
```
[info] All tests passed.
[success] Total time: 127 s, completed Jan 31, 2023 9:58:09 AM
Tst anomaly_detection_dnn_1_0 succeeded:False PCU:55 PMU:87 genpir gentst [86s] make p2p proute hybrid
PCUs: 55
PMUs: 87
Model score: 57.096634033151204
Current optimum: 81.32483505513115
HyperMapper optimization finished
Current optimum: 81.32483505513115
```
