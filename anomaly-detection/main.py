# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import os
import sys
import json
from homunculus import Homunculus
from homunculus.alchemy import DataLoader, Model
from homunculus.backends import Dummy
import loader
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--train-dataset", type=str, required=True)
parser.add_argument("--test-dataset", type=str, required=True)
parser.add_argument("--backend", type=str, required=True)
parser.add_argument("--backend-type", type=str, required=True)
args = parser.parse_args()


@DataLoader
def wrapper_func():
    tnx, tny = loader.load_from_file(args.train_dataset)
    tsx, tsy = loader.load_from_file(args.test_dataset)
    return {
        "data": {
                "train": tnx,
                "test": tsx
        },
        "labels": {
            "train": tny,
            "test": tsy
        }
    }


def load_config(config_file):
    with open(config_file, 'r') as cf:
        return json.load(cf)


ad = Model({
    "algorithm": ["dnn"],
    "optimization_metric": ["f1"],
    "name": "anomaly_detection",
    "data_loader": wrapper_func,
    "config": {"dnn": load_config("config.json")}
})


h = Homunculus()
platform = h.installBackend(args.backend, args.backend_type)()
platform.schedule(ad)

h.generate(platform)
