# Botnet Detection

To mitigate botnet attacks on a per-packet basis using the FlowLens, NDSS '21 dataset.

## Usage

- From within the Python3 virtual environment, run the following commands:
```
cd applications/botnet-detection/
python main.py \
    --train-dataset ~/datasets/flowlens-flowmarkers-traces/Dataset_64_512.csv \
    --test-dataset ~/datasets/flowlens-flowmarkers-traces/PerPktHist_Subset10k_24k_64_512.csv \
    --backend ~/backends/taurus-asic \
    --backend-type TaurusASIC
```

- On successful completion, it will print following output:
```
[info] All tests passed.
[success] Total time: 134 s, completed Jan 31, 2023 10:17:49 AM
Tst botnet_detection_dnn_1_0 succeeded:False PCU:45 PMU:94 genpir gentst [94s] make p2p proute hybrid
PCUs: 45
PMUs: 94
Model score: 72.61788147566104
Current optimum: 72.61788147566104
HyperMapper optimization finished
Current optimum: 72.61788147566104
```

