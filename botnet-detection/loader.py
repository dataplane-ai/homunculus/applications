# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from sklearn.model_selection import train_test_split
from itertools import accumulate
import numpy as np
import csv

# Loader for (modified) FlowLens Botnet Detection Dataset - https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/flowlens-flowmarkers-traces

PL_HIST_LENGTH  = 46
IPT_HIST_LENGTH = 56
labels_to_class = {
    "benign":       [1, 0],
    "malicious":    [0, 1]
}


def load_from_file(dataset, shift):
    with open(dataset) as dataset_file:
        print("Loading Dataset: {0} ...".format(dataset))
        attributes, labels = [], []
        csv_reader = csv.reader(dataset_file)
        for n, row in enumerate(csv_reader):
            if(n == 0):
                continue
            else:
                try:
                    label_class = labels_to_class[row[-1]]
                except:
                    print("UNKNOWN LABEL FOUND IN THIS ROW = {0}".format(row))
                    continue
                attributes.append(list(map(int, row[shift:-1])))
                labels.append(label_class)
        
        print("Dataset size: Attributes = {0}, Labels = {1}".format(np.asarray(attributes).shape, np.asarray(labels).shape))
        print(type(attributes), type(attributes[0]))
        print(type(labels), type(labels[0]))
        return attributes, labels