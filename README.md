# Applications for the Homunculus Framework

In this repository, we share the source code for the various applications for in-network ML using the Homunculus framework. It contains applications from network security to traffic engineering to packet analysis.

- [Anomaly Detection](anomaly_detection): To identify anomalous packets using the [NSL KDD Dataset](https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/kdd-synthetic-traces)
- [Traffic Classification](traffic_classification): To classify packets based on the [TMC IoT Dataset](https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/tmc-iot-traces)
- [Botnet Detection](botnet_detection): To mitigate botnet attacks on a per-packet basis using the [FlowLens, NDSS '21 dataset](https://gitlab.com/dataplane-ai/homunculus/datasets/-/tree/main/flowlens-flowmarkers-traces).

## Usage

- First, clone the Homunculus compiler, Taurus ASIC backend, datasets, and the applications repositories.
```
$ cd ~/
$ git clone https://gitlab.com/dataplane-ai/homunculus/compiler.git
$ git clone https://gitlab.com/dataplane-ai/homunculus/backends/taurus-asic.git
$ git clone https://gitlab.com/dataplane-ai/homunculus/datasets.git
$ git clone https://gitlab.com/dataplane-ai/homunculus/applications.git
```

- Second, set up the Python3 virtual environment and install the Homunculus compiler as instructed [here](https://gitlab.com/dataplane-ai/homunculus/compiler).

- Third, from within the virtual environment, go to the Taurus ASIC repository and install the backend as instructed [here](https://gitlab.com/dataplane-ai/homunculus/backends/taurus-asic).

- Finally, run the applications as instructed in their respective folders (e.g., [anomaly-detection](anomaly-detection)).


## Contact Us 
- [Tushar Swamy](https://www.linkedin.com/in/tushar-swamy-b4aa51b1/)
- [Annus Zulfiqar](https://annusgit.github.io/)
- [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- [Kunle Olukotun](http://arsenalfc.stanford.edu/kunle/)
